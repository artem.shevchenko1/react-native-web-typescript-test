import { useState, useEffect } from 'react';

type fetchFnType<T> = () => Promise<T[]>

export const useGetData = <T>(fetchFn: fetchFnType<T>): [T[], string, boolean] => {
  const [dataList, setDataList] = useState<T[]>([]);
  const [isLoading, setIsLoading] = useState(false);
  const [errorCode, setErrorCode] = useState('');

  useEffect(() => void getData(), []);

  const getData = async () => {
    try {
      setIsLoading(true);
      const fetchedList = await fetchFn();
      setDataList(fetchedList);
    } catch (error) {
      setErrorCode(error.toString());
    } finally {
      setIsLoading(false);
    }
  };

  return [dataList, errorCode, isLoading];
};
