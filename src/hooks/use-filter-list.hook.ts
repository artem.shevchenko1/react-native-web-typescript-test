import { useState, useEffect } from 'react';

type filterListFn<T> = (list: T[], text: string) => T[];
type handleChangeTextFn = (text: string) => void;

export const useFilterList = <T>(fetchedList: T[], filterList: filterListFn<T>): [T[], handleChangeTextFn] => {
  const [list, setList] = useState<T[]>([]);
  
  useEffect(() => void setList(fetchedList), [fetchedList]);

  const handleChangeText = (text: string) => {
    setList(filterList(fetchedList, text));
  }

  return [list, handleChangeText];
};

