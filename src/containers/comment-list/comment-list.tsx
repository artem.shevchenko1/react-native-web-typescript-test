import React, { FC } from 'react';
import { View, Text, FlatList } from 'react-native';
import { InputForm } from '../input-form/input-form';
import { IComment } from '../../interfaces/comment.interface';
import { useGetData } from '../../hooks/use-get-data.hook';
import { useFilterList } from '../../hooks/use-filter-list.hook';
import { fetchComments } from '../../services/comment.service';
import { CommentListStyles } from './comment-list.styles';
import { filterComments } from './comment-list.utils';
import { isEmptyString } from '../../utils/is-empty-string';
import { Loader } from '../../components/loader/loader';

export const CommentList: FC = () => {
  const [fetchedList, error, isLoading] = useGetData<IComment>(fetchComments);
  const [comments, onChangeText] = useFilterList<IComment>(fetchedList, filterComments);

  return (
    <View style={CommentListStyles.containerList}>
      <InputForm onChangeText={onChangeText} />
      {!isEmptyString(error) && <Text>{error}</Text>}
      <Loader isLoading={isLoading}>
        <FlatList
          data={comments}
          renderItem={({ item }) => <Text>{item.name}</Text>}
          keyExtractor={({ id }) => id.toString()}
        />
      </Loader>
    </View>
  );
} 
