import { IComment } from '../../interfaces/comment.interface';

export const filterComments = (list: IComment[], text: string) => list.filter(item => item.name.includes(text));