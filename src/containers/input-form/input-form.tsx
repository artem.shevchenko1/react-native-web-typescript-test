import React, { useState, FC } from "react";
import { View, TextInput, Text } from "react-native";
import { InputFormStyles } from './input-form.styles';
import { InputFormSelectors } from './input-form.selectors';

type onChangeTextFn = (text: string) => void;

interface Props {
  onChangeText: onChangeTextFn;
}

export const InputForm: FC<Props> = ({ onChangeText }) => {
  const [value, setValue] = useState("");

  const handleChangeInput = (text: string) => {
    setValue(text);
    onChangeText(text);
  };

  return (
    <View style={InputFormStyles.container}>
      <Text style={InputFormStyles.labelText}>Search: </Text>
      <View style={InputFormStyles.inputContainer}>
        <TextInput
          testID={InputFormSelectors.Input}
          style={InputFormStyles.inputText}
          value={value}
          placeholder="Type some text"
          onChangeText={handleChangeInput}
        />
      </View>
    </View>
  );
};
