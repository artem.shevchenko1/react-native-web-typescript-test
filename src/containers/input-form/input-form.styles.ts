import { StyleSheet } from 'react-native';

export const InputFormStyles = StyleSheet.create({
  container: {
    alignItems: 'center'
  },
  labelText: {
    padding: 6
  },
  inputContainer: {
    borderWidth: 1,
    marginBottom: 6,
    width: 180
  },
  inputText: {
    padding: 10
  },
});
