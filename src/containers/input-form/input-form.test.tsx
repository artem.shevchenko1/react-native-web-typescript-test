import React from "react";
import { render, cleanup, fireEvent } from '@testing-library/react-native';
import { InputForm } from './input-form';
import { InputFormSelectors } from './input-form.selectors';
import renderer from 'react-test-renderer';

describe('InputForm test', () => {
  const mockOnChangeText = jest.fn();

  it('should renders InputForm correctly', () => {
    const tree = renderer.create(<InputForm onChangeText={mockOnChangeText} />).toJSON();
    expect(tree).toMatchSnapshot();
  });

  it('should trigger onChangeText when text changes', () => {
    const {getByTestId} = render(<InputForm onChangeText={mockOnChangeText}/>);
    const mockText = 'a';

    fireEvent.changeText(getByTestId(InputFormSelectors.Input), mockText);

    expect(mockOnChangeText).toHaveBeenCalledWith(mockText);
  });

  afterEach(cleanup);
});
