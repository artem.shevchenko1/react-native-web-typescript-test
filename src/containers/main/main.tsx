import React, { memo, FC } from "react";
import { View } from "react-native";
import { MainStyles } from "./main.styles";
import { PostList } from '../post-list/post-list';
import { CommentList } from "../comment-list/comment-list";

export const Main: FC = memo(function Main() {
  return (
    <View style={MainStyles.containerMain}>
      <PostList />
      <CommentList />
    </View>
  );
});
