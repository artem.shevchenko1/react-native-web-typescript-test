import { StyleSheet } from 'react-native';

export const MainStyles = StyleSheet.create({
  containerMain: {
    flexDirection: 'row',
    justifyContent: 'center',
    margin: 10
  },
});
