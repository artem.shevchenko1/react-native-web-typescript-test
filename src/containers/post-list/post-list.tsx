import React, { FC } from 'react';
import { View, Text, FlatList } from 'react-native';
import { InputForm } from '../input-form/input-form';
import { useGetData } from '../../hooks/use-get-data.hook';
import { useFilterList } from '../../hooks/use-filter-list.hook';
import { IPost } from '../../interfaces/post.interface';
import { fetchPosts } from '../../services/post.service';
import { PostListStyles } from './post-list.styles';
import { filterPosts } from './post-list.utils';
import { isEmptyString } from '../../utils/is-empty-string';
import { Loader } from '../../components/loader/loader';

export const PostList:FC = () => {
  const [fetchedList, error, isLoading] = useGetData<IPost>(fetchPosts);
  const [posts, onChangeText] = useFilterList<IPost>(fetchedList, filterPosts);

  return (
    <View style={PostListStyles.containerList}>
      <InputForm onChangeText={onChangeText} />
      {!isEmptyString(error) && <Text>{error}</Text>}
      <Loader isLoading={isLoading} >
        <FlatList
          data={posts}
          renderItem={({ item }) => <Text>{item.title}</Text>}
          keyExtractor={({ id }) => id.toString()}
        />
      </Loader>
    </View>
  );
};
