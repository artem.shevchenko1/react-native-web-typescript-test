import { IPost } from '../../interfaces/post.interface';

export const filterPosts = (list: IPost[], text: string) => list.filter(item => item.title.includes(text));
