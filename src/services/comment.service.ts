import { fetchList } from '../utils/fetch-data';
import { IComment } from '../interfaces/comment.interface';
import { COMMENTS_URL } from '../url.config';

export const fetchComments = async (): Promise<IComment[]> => fetchList<IComment>(COMMENTS_URL);
