import { fetchList } from '../utils/fetch-data';
import { IPost } from '../interfaces/post.interface';
import { POSTS_URL } from '../url.config';

export const fetchPosts = async (): Promise<IPost[]> => fetchList<IPost>(POSTS_URL);
