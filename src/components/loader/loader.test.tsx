import React from 'react';
import { render, cleanup } from '@testing-library/react-native';
import { Loader } from './loader';
import {Text} from 'react-native';
import { LoaderSelectors } from './loader.selectors';

describe('Loader test', () => {
  afterEach(cleanup);

  const mockChildren = <Text>good</Text>;

  it('should shows Loading... text when isLoading prop is true', () => {
    const {getByTestId} = render(
      <Loader isLoading={true}>{mockChildren}</Loader>
    );

    expect(getByTestId(LoaderSelectors.Loading).props.children).toBe('Loading...');
  });

  it('should shows children prop when isLoading prop is false', () => {
    const {getByText} = render(
      <Loader isLoading={false}>{mockChildren}</Loader>
    );

    expect(getByText('good').props.children).toBeTruthy();
  });
});
