import React, { FC } from 'react';
import { Text, View } from 'react-native';
import { LoaderSelectors } from './loader.selectors';

interface Props {
  isLoading: boolean;
}

export const Loader: FC<Props> = ({ isLoading, children }) => 
  (
    <>
      {
        isLoading
          ? <Text testID={LoaderSelectors.Loading}>Loading...</Text>
          : <>{children}</>
      }
    </>
  )