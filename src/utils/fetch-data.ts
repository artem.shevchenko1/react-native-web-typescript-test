export const fetchList = async <T>(url: string) : Promise<T[]> => {
  try {
    const response = await fetch(url);

    if (response.status === 404) {
      throw new Error(`${response.status}`);
    }
  
    const fetchedList = await response.json();
    return fetchedList;
  } catch(error) {
    throw error;
  }
}
